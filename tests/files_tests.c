/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <libgen.h>
#include <unity.h>

#include "../../src/include/files.h"

void
setUp (void)
{
}

void
tearDown (void)
{
}

void
test_has_first_configuration_name (void)
{
  GList *last_cfg = g_list_last (configs_found ());
  char *actual = basename ((g_list_nth (last_cfg, 0))->data);
  char *expected = "no_branch_project.json";
  TEST_ASSERT_EQUAL_STRING (actual, expected);
}

void
test_has_all_configurations (void)
{
  guint configs_len = g_list_length (configs_found ());
  TEST_ASSERT_EQUAL_INT (configs_len, 4);
}
