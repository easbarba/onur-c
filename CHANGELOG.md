<!--
Onur is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Onur is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Onur. If not, see <https://www.gnu.org/licenses/>.
-->

# CHANGELOG

## 0.6.0
 - refactor: tests, pita library, and more

## 0.5.2
 - feat: filter configurations in grab command

## 0.5.1
 - feat: implement long options flags
 - feat: call description in cli help option
 - feat: check projects fields

## 0.4.1
- ops: add meson wrap dependencies
- feat:  leaner output
- refactor: use gnome glib and improve codebase

## 0.4.0
- feat: new configuration with subtopics

## 0.3.0
- feat: parse configurations
- feat: add grab command
- feat: add pull and klone actions
- fix: overall refactor

## 0.2.1
- feat: improve list configs, refactor, fmt, lint

## 0.2.0

- feat: list configurations
- feat: add cli parsing

# 0.1.0

- initial structure
