/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "include/cli.h"
#include "include/commands.h"
#include "include/settings.h"

static void usage (void);
// main cli parsing handler
void
cli_run (int argc, char *argv[])
{
  char *subcommand = argv[1];
  char *filter_opt = "";

  if (argc < 2)
    usage ();

  if (strcmp (subcommand, "grab") == 0)
    {
      configuration_foreach (filter_opt, CMD_GRAB);
      exit (EXIT_SUCCESS);
    }

  if (strcmp (subcommand, "archive"))
    {
      configuration_foreach (filter_opt, CMD_ARCHIVE);
      exit (EXIT_SUCCESS);
    }

  if (strcmp (subcommand, "config"))
    {
      configuration_foreach (filter_opt, CMD_CONFIG);
      exit (EXIT_SUCCESS);
    }

  usage ();
}

void
usage (void)
{
  fprintf (
      stdout,
      "Usage: onur <command> [flags]\n\n"
      "Commands:\n"
      "  archive <package> [flags]                       archive selected "
      "projects\n"
      "  config <topic> [<name> [<url> [<branch>]]]      manage "
      "configurations\n"
      "  grab [<topic>] [flags]                          grab all projects\n\n"
      "Run 'onur <command> --help' for more information on a command.");

  exit (EXIT_SUCCESS);
}

void
cli_run_old (int argc, char *argv[])
{
  char short_options[7] = "ga:f:h";
  static struct option long_options[]
      = { { "grab", no_argument, 0, 'g' },
          { "archive", required_argument, 0, 'a' },
          { "filter", required_argument, 0, 'f' },
          { "help", no_argument, 0, 'h' },
          { 0, 0, 0, 0 } };

  bool grab_opt = false;
  char *filter_option = nullptr;
  char *archive_opt = nullptr;

  int option;
  while (
      (option = getopt_long (argc, argv, short_options, long_options, nullptr))
      != -1)
    {
      switch (option)
        {
        case 'f':
          filter_option = optarg;
          break;
        case 'g':
          grab_opt = true;
          break;
        case 'a':
          archive_opt = optarg;
          break;
        case 'h':
          usage ();
          break;
        default:
          usage ();
        }
    }

  // No options have been passed
  if (optind == 1)
    usage ();

  settings_print ();

  if (grab_opt == true)
    configuration_foreach (filter_option, CMD_GRAB);

  if (archive_opt != nullptr)
    configuration_foreach (filter_option, CMD_ARCHIVE);
}

void
description_old (void)
{
  fprintf (stdout,
           "Usage: onur [GLOBAL OPTIONS] [OPTIONS]\n"
           "\n Global options:\n"
           "  -f, --filter <config_name>    filter configuration\n"
           "\n Options:\n"
           "  -g, --grab                    grab all projects\n"
           "  -a, --archive <projects>      archive projects listed\n"
           "  -h, --help                    Display this help message\n");

  exit (EXIT_SUCCESS);
}
