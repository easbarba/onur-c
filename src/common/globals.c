/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/globals.h"

static char *home_path (void);

// user's home directory path
char *
home_path (void)
{
  char *result = getenv ("HOME");
  if (result == nullptr)
    {
      fprintf (stderr, "Error: $HOME is not set, exiting!");
      exit (EXIT_FAILURE);
    }

  return result;
}

// onur configuration directory path
char *
onur_path (void)
{
  char *_home_path = home_path ();

  int onur_home_size = 13; // sizeof ("/.config/onur");
  unsigned long result_size = strlen (_home_path) + onur_home_size + 1;
  char *result = malloc (result_size);
  if (result == nullptr)
    {
      fprintf (stderr, "malloc failed at onur_path()\n");
      exit (EXIT_FAILURE);
    }

  int ret = snprintf (result, result_size, "%s/%s/%s", _home_path, ".config",
                      "onur");
  if (ret == false)
    {
      fprintf (stderr, "unable to build Onur configuration Home Path\n");
      exit (EXIT_FAILURE);
    }

  return result;
}

// onur project directory path
char *
onur_projects_path (void)
{
  char *_home_path = home_path ();
  int projects_home_size = 10; // sizeof("/Projects")
  char *result = malloc (sizeof (_home_path) + projects_home_size + 1);
  if (result == nullptr)
    {
      fprintf (stderr, "malloc failed on onur_projects_path()\n");
      exit (EXIT_FAILURE);
    }

  int ret = snprintf (result, projects_home_size + sizeof (home_path ()) + 10,
                      "%s/%s", _home_path, "Projects");
  if (ret == false)
    {
      fprintf (stderr, "unable to build Projects Home Path\n");
      exit (EXIT_FAILURE);
    }

  return result;
}
