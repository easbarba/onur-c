/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <glib-2.0/glib.h>
#include <ini.h>

#include <pita_bool.h>

#include "../include/settings.h"

static int handler (void *user, const char *section, const char *name,
                    const char *value);

static int
handler (void *user, const char *section, const char *name, const char *value)
{
  struct Settings *settings = (struct Settings *)user;

  if (strcmp (section, "git") == 0)
    {
      if (strcmp (name, "depth") == 0)
        {
          settings->depth = atoi (value);
        }
      else if (strcmp (name, "single_branch") == 0)
        {
          settings->single_branch
            = (strcmp (value, "true") == 0 || strcmp (value, "1") == 0);
        }
      else if (strcmp (name, "quiet") == 0)
        {
          settings->quiet
            = (strcmp (value, "true") == 0 || strcmp (value, "1") == 0);
        }
      else
        {
          return 0; // Unknown key
        }
    }
  else
    {
      return 0; // Unknown section
    }
  return 1;
}

struct Settings
settings_value (void)
{
  struct Settings settings
      = { .single_branch = true, .quiet = true, .depth = 1 };

  const char *home = g_get_user_config_dir ();
  char *filepath = g_build_path ("/", home, "onur", "settings.toml", nullptr);

  if (ini_parse (filepath, handler, &settings) < 0)
    return settings;

  return settings;
}

// BOOLEAN

void
settings_print (void)
{
  struct Settings settings = settings_value ();
  fprintf (stdout, "Settings: [ single branch: %s - quiet: %s - depth: %d ]\n",
           pita_bool_stringfy (settings.single_branch),
           pita_bool_stringfy (settings.quiet), settings.depth);
}
