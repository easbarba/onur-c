/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/project.h"

Project *
project_new (char *name, char *url, char *branch)
{
  Project *project = malloc (sizeof (Project *) + sizeof (char *) * 3);
  if (project == nullptr)
    {
      perror ("Error: failed to allocate memory to Project struct!");
      exit (EXIT_FAILURE);
    }

  project->name = name;
  project->url = url;
  project->branch = branch;

  return project;
}

void
project_delete (Project *project)
{
  free (project->name);
  project->name = NULL;

  free (project->url);
  project->url = NULL;

  /* free (project->branch); // TODO */
  project->branch = NULL;

  free (project);
  project = NULL;
}
