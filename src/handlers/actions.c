/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <pita_system.h>

#include "../include/actions.h"
#include "../include/settings.h"

static char *grabber = "git";

void
klone (const struct Project *project, char *fullpath)
{
  struct Settings settings = settings_value ();

  char *depth = malloc (sizeof (settings.depth) + 9);
  int ret = snprintf (depth, sizeof (settings.depth) + 9, "--depth=%d",
                      settings.depth);
  if (ret == false)
    {
      fprintf (
          stderr,
               "Error occured on depth concatenation, defaulting to '--depth=1'");
      depth = "--depth=1";
    }

  char *final_cmd[]
    = { grabber, "clone",      "--single-branch", "--no-tags", "--quiet",
        depth,   project->url, fullpath,          NULL };
  pita_run_cmd (final_cmd, false);
}

void
pull (const struct Project *project, char *fullpath)
{
  char *final_cmd[]
      = { grabber,         "-C",      fullpath,    "fetch", "origin",
          project->branch, "--quiet", "--depth=1", NULL };
  pita_run_cmd (final_cmd, false);

  char *final_cmd_reset[] = { grabber,  "-C",          fullpath,  "reset",
                              "--hard", "origin/HEAD", "--quiet", NULL };
  pita_run_cmd (final_cmd_reset, false);
}
