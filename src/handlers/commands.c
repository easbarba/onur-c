/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib-2.0/glib.h>

#include "pita_file.h"
#include "pita_string.h"

#include "../include/actions.h"
#include "../include/commands.h"
#include "../include/config.h"
#include "../include/globals.h"
#include "../include/project.h"
#include "../include/repository.h"

static void *project_path_build (void *, void *);
static void grab_command (GList *projects, char *root);
static void archive_command (char *projects);

void
config_info (gpointer data, gpointer user_data)
{
  GList *configs = data;
  if (configs == nullptr)
    {
      printf ("nulo");
      exit (0);
    }
  printf ("configurations(%d): [", g_list_length (configs));
  for (GList *configurations = configs; configurations != nullptr;
       configurations = configurations->next)
    {
      Config *configuration = (Config *)configurations->data;
      fprintf (stdout, "\n> %s\n", configuration->name);
    }

  printf (" ]");
  (void)user_data;
}

// cycle through configurations running action given
void
configuration_foreach (char *filter_term, Commands command)
{
  GList *_all_configs = all_configs ();

  /* g_list_foreach (_all_configs, config_info, nullptr); */

  for (GList *configurations = _all_configs; configurations != nullptr;
       configurations = configurations->next)
    {
      Config *configuration = (Config *)configurations->data;
      char *_configuration_name = configuration->name;
      printf ("> %s \n", _configuration_name);
      if (filter_term != nullptr
          && pita_string_starts_with (filter_term, _configuration_name, 0, 0))
        continue;

      GHashTable *_configuration_topics = configuration->topics;
      GList *config_topics = g_hash_table_get_keys (_configuration_topics);
      for (GList *_topics = config_topics; _topics != nullptr;
           _topics = _topics->next)
        {
          void *topic = _topics->data;
          GList *_projects
              = g_hash_table_lookup (_configuration_topics, topic);

          fprintf (stdout, "%2s+ %s (%d)\n", "", (char *)topic,
                   g_list_length (_projects));

          char *root = project_path_build (_configuration_name, topic);

          switch (command)
            {
            case CMD_GRAB:
              grab_command (_projects, root);
              break;
            case CMD_ARCHIVE:
              archive_command (nullptr); // TODO:
              break;
            case CMD_CONFIG:
              archive_command (nullptr); // TODO:
              break;
            }

          fprintf (stdout, "\n");
          g_list_free (_projects);
        }

      g_list_free (config_topics);
      g_hash_table_destroy (configuration->topics);
    }

  g_list_free (_all_configs);
}

static void
grab_command (GList *projects, char *root)
{
  for (GList *ii = projects; ii != nullptr; ii = ii->next)
    {
      Project *project = ii->data;
      void *project_path = g_build_path ("/", root, project->name, nullptr);

      fprintf (stdout, "%4s- %-35s %-70s %s\n", "", project->name,
               project->url, project->branch);

      if (pita_file_exists (project_path))
        pull (project, project_path);
      else
        klone (project, project_path);

      free (project_path);
      project_path = nullptr;

      /* g_free (project->branch); */
      /* g_free (project->url); */
      /* g_free (project->name); */

      project_delete (project);
      project = nullptr;
    }
}

static void
archive_command (char *projects)
{
  if (projects == nullptr)
    {
      fprintf (stderr, "error: no project has been given");
      exit (EXIT_FAILURE);
    }

  fprintf (stdout, "backing up: %s", projects);
}

// returns absolute path of project
static void *
project_path_build (void *topic, void *subtopic)
{
  void *_onur_projects_path = onur_projects_path ();
  if (_onur_projects_path == nullptr)
    {
      fprintf (stderr, "Error: Cant properly set Onur's Project folder.");
      exit (EXIT_FAILURE);
    }

  void *result
      = g_build_path ("/", _onur_projects_path, topic, subtopic, nullptr);

  free (_onur_projects_path);
  _onur_projects_path = nullptr;

  return result;
}
