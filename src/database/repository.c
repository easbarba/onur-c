/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <bits/stdint-uintn.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <json.h>
#include <json_object.h>
#include <json_tokener.h>
#include <json_types.h>
#include <json_util.h>

#include <pita_path.h>
#include <pita_string.h>

#include "../include/config.h"
#include "../include/files.h"
#include "../include/project.h"
#include "../include/repository.h"
#include "glib.h"

static struct json_object *json_check_main_object (const char *filepath);
/* static void *read_file (const void *filepath); */
static GHashTable *parse_file (const void *filepath);
static Config *one_config (const char *filepath);

Config *
one_config (const char *filepath)
{

  if (filepath == nullptr)
    return nullptr;

  Config *result = g_new (struct Config, 1);
  result->name = pita_path_stem (filepath);
  result->topics = parse_file (filepath);

  if (!result->topics)
    return nullptr;

  return result;
}

// list of all parsed configuration
GList *
all_configs (void)
{
  GList *result = nullptr;
  GList *configs = configs_found ();

  for (GList *i = configs; i != nullptr; i = i->next)
    {
      char *filepath = (void *)i->data;

      Config *config = one_config (filepath);
      if (config == nullptr)
        break;

      result = g_list_append (result, config);
    }

  g_list_free (configs);

  return result;
}

GHashTable *
parse_file (const void *filepath)
{
  GHashTable *result = g_hash_table_new (g_str_hash, g_str_equal);

  struct json_object *config_parsed = json_check_main_object (filepath);
  if (config_parsed == nullptr)
    return nullptr;

  // Topics
  json_object_object_foreach (config_parsed, key, val)
  {
    char *topic_name = key;
    struct json_object *topic_projects = val;

    // Topics -> Projects
    GList *projects_parsed = nullptr;
    for (size_t i = 0; i < json_object_array_length (topic_projects); i++)
      {
        struct json_object *project_raw
            = json_object_array_get_idx (topic_projects, i);
        if (!json_object_is_type (project_raw, json_type_object))
          {
            fprintf (stderr, "error: project should be an object %s\n",
                     (const char *)filepath);
            break;
          }

        if (json_object_object_length (project_raw) == 0)
          {
            fprintf (stderr,
                     "error: project object must contain at least name and "
                     "url: %s\n",
                     (const char *)filepath);
            break;
          }

        // NAME
        const char *name_raw = json_object_get_string (
            json_object_object_get (project_raw, "name"));

        if (json_object_object_get (project_raw, "name") == nullptr)
          {
            fprintf (stderr,
                     "error: project object must have key: 'name', and its "
                     "value should be a string: %s\n",
                     (const char *)filepath);
            break;
          }

        // URL
        const char *url_raw = json_object_get_string (
            json_object_object_get (project_raw, "url"));

        if (json_object_object_get (project_raw, "url") == nullptr
            || json_object_is_type (
                   json_object_object_get (project_raw, "url"),
                   json_type_string)
                   == false)
          {
            fprintf (stderr,
                     "error: project object must have a key: 'url', and its "
                     "value should be a valid url STD66 (RFC3986): %s\n",
                     (const char *)filepath);
            break;
          }

        /* if (!g_uri_is_valid (url, G_URI_FLAGS_NONE, nullptr)) */
        /* continue; */

        // BRANCH
        const char *branch_raw = json_object_get_string (
            json_object_object_get (project_raw, "branch"));

        // Trim space around strings
        char *name = pita_string_trim (name_raw);
        char *url = pita_string_trim (url_raw);
        char *branch
            = json_object_object_get (project_raw, "branch") != nullptr
                  ? pita_string_trim (branch_raw)
                  : "master";

        Project *project = project_new (name, url, branch);
        projects_parsed = g_list_append (projects_parsed, project);
      }

    g_hash_table_insert (result, topic_name, projects_parsed);
  }

  free (config_parsed);
  config_parsed = nullptr;

  return result;
}

struct json_object *
json_check_main_object (const char *filepath)
{
  json_object *result = json_object_from_file (filepath);
  if (result == nullptr)
    {
      fprintf (stderr,
               "error: parsing configuration file had some issue: %s\n",
               (const char *)filepath);
      return nullptr;
    }

  if (!json_object_is_type (result, json_type_object))
    {
      fprintf (stderr, "error: configuration should be a json object: %s\n",
               (const char *)filepath);
      return nullptr;
    }

  return result;
}
