/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "glib.h"

#include "pita_file.h"
#include "pita_path.h"
#include "pita_string.h"

#include "include/files.h"
#include "include/globals.h"

/* static gint compare_names (gconstpointer a, gconstpointer b); */

// 0 to ignore file
int
filter (const struct dirent *entry)
{
  const char *filename = entry->d_name;
  char *onurpath = onur_path ();
  char *filepath = pita_path_join_simple (onurpath, filename);

  if (!pita_string_has_extension (filename, ".json"))
    {
      free (onurpath);
      free (filepath);

      return 0;
    }

  if (pita_file_exists (filepath))
    {
      free (onurpath);
      free (filepath);

      return 0;
    }

  if (pita_file_is_empty (filepath))
    {
      free (onurpath);
      free (filepath);

      return 0;
    }

  return 1;
}

// Compare function to sort entries alphabetically
int
compare_alpha (const struct dirent **first, const struct dirent **second)
{
  return strcmp ((*first)->d_name, (*second)->d_name);
}

GList *
configs_found (void)
{
  GList *result = nullptr;
  char *onurpath = onur_path ();
  struct dirent **namelist;

  int entries = scandir (onurpath, &namelist, filter, compare_alpha);
  if (entries == -1)
    {
      fprintf (stderr, "error: could not scan onur directory!\n");
      exit (EXIT_FAILURE);
    }

  for (int i = 0; i < entries; i++)
    {
      char *filepath = pita_path_join_simple (onurpath, namelist[i]->d_name);
      result = g_list_append (result, strdup (filepath));

      free (filepath);
      free (namelist[i]);
    }
  /* result = g_list_sort (result, compare_alpha); */

  free (onurpath);
  free (namelist);

  return result;
}
