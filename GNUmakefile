# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

# DEPS: gcc meson ninja muon coreutils valgrind indent splint cunit

.DEFAULT_GOAL: test

RUNNER := podman
SHELL = bash -O extglob
PREFIX     ?= ${HOME}/.local
NAME       = onur
VERSION    := $(shell cat src/version)
IMAGES_REGISTRY := registry.gitlab.com
IMAGE_NAME := ${IMAGES_REGISTRY}/${USER}/${NAME}-c:${VERSION}

RM         = rm -rf
BUILDDIR = ./build
DEPS = ./subprojects

# ------------------------------------ IMAGE

.PHONY: image.build
image.build:
	$(RUNNER) build -f Containerfile --tag ${IMAGE_NAME}

.PHONY: image.tests
image.tests:
	${RUNNER} run --rm -it ${IMAGE_NAME}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${IMAGE_NAME}

.PHONY: image.grab
image.grab:
	${RUNNER} run --rm -it ${IMAGE_NAME} bash -c "$(BUILDDIR)/src/$(NAME) grab"

# ------------------------------------ LOCAL

.PHONY: all
all: local.clean local.setup local.compile

.PHONY: dev
all: local.clean local.setup.dev local.compile

.PHONY: local.setup.deps
local.setup.deps:
	meson subprojects download

.PHONY: local.setup
local.setup: local.setup.deps
	CC=gcc meson setup ${BUILDDIR} --wipe -D b_sanitize=none -D buildtype=release --default-library=static

.PHONY: local.setup.dev
local.setup.dev:
	CC=gcc meson setup ${BUILDDIR} --wipe -D b_sanitize=none -D buildtype=release

.PHONY: local.dev.setup
local.dev.setup: local.clean
	CC=gcc meson setup ${BUILDDIR} --wipe

.PHONY: local.compile
local.compile:
	meson compile -C $(BUILDDIR)

.PHONY: local.test
local.test:
	${RUNNER} run --rm -it  ${IMAGE_NAME}

.PHONY: local.muon
local.muon:
	CC=gcc muon-meson setup $(BUILDDIR)
	ninja -C $(BUILDDIR)

.PHONY: local.clean.deps
local.clean.deps:
	@rm -rf ${DEPS}/!(*.wrap|packagefiles)

.PHONY: local.clean
local.clean: local.clean.deps
	@$(RM) $(BUILDDIR)
	@$(RM) .cache

.PHONY: local.install
local.install:
	cp -v ${BUILDDIR}/src/${NAME} ${PREFIX}/bin/${NAME}
	mkdir -pv ${PREFIX}/lib
	cp -v ${BUILDDIR}/subprojects/pita/libpita.so ${PREFIX}/lib

.PHONY: local.uninstall
local.uninstall:
	rm ${PREFIX}/${NAME}

.PHONY: local.format
local.format:
	indent -l 80 -gnu -nut -bl  src/*.c
	muon-meson fmt -i ./meson.build

.PHONY: local.lint
local.lint:
	splint -preproc -unrecog -warnposix src/*.c

.PHONY: local.bugs
local.bugs: local.compile
	gdb ${BUILDDIR}/src/onur

.PHONY: local.leaks
local.leaks:
	valgrind --leak-check=full \
		 --show-leak-kinds=all \
		 --track-origins=yes \
		 --verbose \
		 $(BUILDDIR)/src/onur --grab
		 # --log-file=valgrind-output \

# ------------------------------------ ACTIONS

.PHONY: local.actions.usage
local.actions.usage:
	$(BUILDDIR)/src/$(NAME)

.PHONY: local.actions.grab
local.actions.grab:
	$(BUILDDIR)/src/$(NAME) grab

.PHONY: local.actions.archive
local.actions.archive:
	$(BUILDDIR)/src/$(NAME) archive meh,foo,bar

.PHONY: local.actions.config
local.actions.config:
	${BUILDDIR}/src/$(NAME) config c.tools ht https://github.com/benhoyt/ht master
