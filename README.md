<!--
Onur is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Onur is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Onur. If not, see <https://www.gnu.org/licenses/>.
-->

# Onur | C23

Easily manage multiple FLOSS repositories.

[python](https://gitlab.com/easbarba/onur-python) | [dotnet](https://gitlab.com/easbarba/onur-dotnet) | [go](https://gitlab.com/easbarba/onur-go) | [rust](https://gitlab.com/easbarba/onur-rust) | [cpp](https://gitlab.com/easbarba/onur) | [php](https://gitlab.com/easbarba/onur-php) | [java](https://gitlab.com/easbarba/onur-java)
| [ruby](https://gitlab.com/easbarba/onur-ruby)

## Usage

```shell
# grab all projects
onur grab

# grab only the c projects
onur grab c

# list the cpp configuration file
onur config cpp

# list topics of haskell
onur config haskell.

# list only the projects on misc topic of lisp
onur config lisp.misc

# add a new configuration with theses entries in the topic misc of c
onur config c.misc cli11 https://github.com/cliutils/cli11 main

# back up these projects as tar.gz
onur backup ecmascript.nuxt lua.awesomewm misc.gitignore

onur --help
```

## Configurations

`onur` consumes configuration files found at `$XDG_CONFIG/onur`, or in the
directory set in the `$ONUR_CONFIG_HOME` environment variable.

Its configurations are in the manner of the openAPI JSON Schema, object embedded by dictionaries, topics, with key as string and values as an array of objects, projects, with properties:

- `name`: a string,
- `url`: URI resources allowed by [Git URL](https://git-scm.com/docs/git-clone#_git_urls), and
- `branch`: an optional string, defaults to `master`


```json
{
  "main": [
	{
	  "name": "awesomewm",
	  "url": "https://github.com/awesomeWM/awesome"
	},
	{
	  "name": "nuxt",
	  "branch": "main",
	  "url": "https://github.com/nuxt/framework"
	}
  ],
  "misc": [
	{
	  "name": "awesomewm",
	  "url": "https://github.com/awesomeWM/awesome"
	},
	{
	  "name": "nuxt",
	  "branch": "main",
	  "url": "https://github.com/nuxt/framework"
	}
  ],
  "tools/gnu": [
	{
	  "name": "inetutils",
	  "url": "https://git.savannah.gnu.org/git/inetutils.git"
	},
	{
	  "name": "gnu-wget",
	  "url": "https://git.savannah.gnu.org/git/wget.git"
	}
  ]
}
```

More examples of configuration files are at `examples`.

## Settings

`onur` settings found to at `$XDG_CONFIG/onur`, or `$ONUR_CONFIG_HOME` environment variable is a ini-like TOML file.

Its an ever grow definitions are found in the (example dir)[example]:

`settings.ini`

```ini
[git]
single-branch = true
quiet = true
depth = 1
```

## Installation

`Onur` requires a [C99](https://gcc.gnu.org/) compiler, the [pita](https://gitlab.com/easbarba/pita) library, and [Meson](https://mesonbuild.com/), then just run `make clean all`, and executable file will be placed at `$PWD/.build/onur`.

Dependencies: [GNU GCC](https://gcc.gnu.org/) - [GNU Make](https://www.gnu.org/software/make/) - [GNOME glib](https://gitlab.gnome.org/GNOME/glib) - [inih](https://github.com/benhoyt/inih) - [libgit2](https://libgit2.org/) - [json-c](https://github.com/json-c/json-c) - [Podman ](https://podman.io/).

Tip: A clean install without messing around your system is easily achievable with [GNU Guix](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html): `guix shell --check`.

## Development

In development it may suit you better running the tests in a isolated environment
with containers, that can be done so:

	docker run --rm -it $(docker build -qf Containerfile.run)

	podman build https://gitlab.com/easbarba/onur/-/raw/main/Containerfile.dev --tag onur:latest
	podman run --rm -it onur:latest


## GNU Guix

In a system with GNU Guix binary installed, its even easier to grab all
dependencies: `guix shell`.

![Onur CLI](onur.png)

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
